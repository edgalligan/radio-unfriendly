(function() {

    var TARGETELEMENT = document.getElementById('cloudcasts');

    var MIXCLOUD = {
        api: {
            process: function(response) {
                var i, l, data, cast, castwrap;

                for (i = 0, l = response.data.length; i<l; ++i) {
                    data = response.data[i];

                    var player = MIXCLOUD.html.buildPlayer(data);
                    var thumb = MIXCLOUD.html.buildThumb(data);
                    var tags = MIXCLOUD.html.processTags(data.tags);
                    var cast = MIXCLOUD.html.buildCast({thumb: thumb, player: player, tags: tags});

                    TARGETELEMENT.appendChild(cast);
                }
            }
        },

        html: {
            buildPlayer: function(data) {
                var link = document.createElement('a');
                link.className = 'castlink';
                link.setAttribute('href', data.url);
                link.textContent = data.name;

                return link;
            },
            buildThumb: function(data) {
                var link = document.createElement('a');
                link.className = 'castthumb';
                link.setAttribute('href', data.pictures.extra_large);

                var img = document.createElement('img');
//                img.setAttribute('src', data.pictures.thumbnail);
                img.setAttribute('src', data.pictures.medium_mobile);

                link.appendChild(img);

                return link;
            },
            processTags: function(tags) {
                var i, l, taglink, tagwrap = document.createElement('div');
                tagwrap.className = 'casttagwrap';
                for (i = 0, l = tags.length; i<l; ++i) {
                    taglink = document.createElement('a');
                    taglink.className = 'casttaglink';
                    taglink.setAttribute('href', tags[i].url);
                    taglink.textContent = tags[i].name;
                    tagwrap.appendChild(taglink);
                }
                return tagwrap;
            },
            buildCast: function(elements) {
                var index, cast = document.createElement('div');
                cast.className = 'castwrap';
                for (index in elements) {
                    cast.appendChild(elements[index]);
                }
                return cast;
            }
        }
    };

    window['mixcloud'] = MIXCLOUD;
})();
